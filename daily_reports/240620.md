# Tag 3 (24.06.2020)

## Aufgaben

### Aufgabe 1

#### Splitstree

* Was bedeuten lange Kanten im Splitstree-Baum?
	* Quelle: Daniel H. Huson.  SplitsTree:  A program for analyzing and visualizing evolutionary data (S. 69)
	* "The length of the edges representing a given split S indicates its weight or support and is calculated as the isolation index of S."
	
	* Quelle:  Daniel H. Huson, David Bryant. Molecular Biology and Evolution, Volume 23, Issue 2, February 2006(S.3)
	* "The length of an edge in the network is proportional to the weight of the associated split. This isanalogous to the length of a branch in a phylogenetic tree."

	* *Notiz: "The 'phenetic  distance' between two taxa in a split network is defined as the sum of the weights(or lengths) of the edges along a shortest path between thetaxa." (Molecular Biology and Evolution)*
	
* Worauf deuten rechteckige oder gar 'quadratische' Strukturen in der Darstellung hin?
	* Quelle: Daniel H. Huson, David Bryant. Molecular Biology and Evolution, Volume 23, Issue 2, February 2006 (S.8)
	* Rechteckige Strukturen beeinflussen die internal branch lengths


### Aufgabe 2 

Wichtige Proteindomains für die eigenen Zielsequenzen.

#### CBX2

* Quelle: genecards (https://www.genecards.org/cgi-bin/carddisp.pl?gene=CBX2&keywords=CBX2#domains_families)
* Protein Domains for CBX2 Gene:
	* Blocks: Chromo domain 
	* InterPro: Chromo-like_dom_sf, Chromo/chromo_shadow_dom, CBX7_C, CBX2, Chromodomain_CS, Chromo_domain
	* ProtoNet: Q14781 
	
#### CBX4

* Quelle: genecards (https://www.genecards.org/cgi-bin/carddisp.pl?gene=CBX4&keywords=CBX4#domains_families)
* Protein Domains for CBX4 Gene
	* Blocks: Chromo domain 
	* InterPro: Chromo-like_dom_sf, Chromo/chromo_shadow_dom, CBX7_C, Chromodomain_CS, Chromo_domain, Chromo_dom_subgr 
	* ProtoNet: O00257 
	
#### CBX6

* Quelle: genecards (https://www.genecards.org/cgi-bin/carddisp.pl?gene=CBX6&keywords=CBX6#domains_families)
* Protein Domains for CBX6 Gene
	* Blocks: Chromo domain 
	* InterPro: Chromo-like_dom_sf, Chromo/chromo_shadow_dom, CBX7_C, Chromodomain_CS, Chromo_domain, Chromo_dom_subgr 
	* ProtoNet: O95503 
	
#### CBX7

* Quelle: genecards (https://www.genecards.org/cgi-bin/carddisp.pl?gene=CBX7&keywords=CBX7#domains_families)
* Protein Domains for CBX7 Gene
	* Blocks: Chromo domain 
	* InterPro: Chromo-like_dom_sf, Chromo/chromo_shadow_dom, CBX7_C, Chromodomain_CS, Chromo_domain, Chromo_dom_subgr, CBX7 
	* ProtoNet: O95931
	
#### CBX8

* Quelle: genecards (https://www.genecards.org/cgi-bin/carddisp.pl?gene=CBX8&keywords=CBX8#domains_families)
* Protein Domains for CBX8 Gene
	* Blocks: Chromo domain 
	* InterPro: Chromo-like_dom_sf, Chromo/chromo_shadow_dom, CBX7_C, Chromodomain_CS, Chromo_domain 
	* ProtoNet: Q9HC52
	
#### RYBP

* Quelle: genecards (https://www.genecards.org/cgi-bin/carddisp.pl?gene=RYBP&keywords=RYBP#domains_families)
* Protein Domains for RYBP Gene
	* Blocks: Zn-finger, Ran-binding 
	* InterPro: Znf_RanBP2_sf, Znf_RanBP2, YAF2_RYBP, RYBP/YAF2 
	* ProtoNet: Q8N488 
	
#### YAF2

* Quelle: genecards (https://www.genecards.org/cgi-bin/carddisp.pl?gene=YAF2&keywords=YAF2#domains_families)
* Protein Domains for YAF2 Gene
	* Blocks: Zn-finger, Ran-binding 
	* InterPro: Znf_RanBP2_sf, Znf_RanBP2, YAF2_RYBP, RYBP/YAF2, YAF2 (IPR038039)
	* ProtoNet: Q8IY57

#### Zusammenfassung: Liste vorkommender Domänen (InterPro)

**Über Pfam downloadbar**

* Chromo-like_dom_sf
* Chromo/chromo_shadow_dom
* **CBX7_C**
* CBX2
* Chromodomain_CS
* **Chromo_domain**
* Chromo_dom_subgr 
* CBX7 
* Znf_RanBP2_sf
* Znf_RanBP2
* **YAF2_RYBP**
* RYBP/YAF2
* YAF2 

	


	










