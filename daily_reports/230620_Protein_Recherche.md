aus Schuettengruber(2017)
- CBX (bei Fliegen Pc) und RYBP/YAF2 Proteine sind Bestandteil des PRC1 Komplex (Repressionskomplex) der Polycomb-Gruppe
- Polycomb- und Trithorax-Gruppen sind evolutionär konserviert
- wirken antagonistisch, sind Chromatin-modifizierend
- PcG-Proteine bilden Multiproteinkomplexe, die durch Modifikation (chromatinbasiert) an Repression der Genexpression beteiligt sind (Gene sind beteiligt an z.B. Entwicklung)

- PRC1 bei Säugetieren umfasst kanonischen Komplex (cPRC1) und nicht kanonischen Komplex (ncPRC1)
- cPRC1 werden um PCGF2 bzw 4 gebildet und werden spezifiziert durch ein Chromobox Protein (CBX2, 4, 6-8) und ein Ph homologes Protein (PHC1-3) 
- das Ph homologe Protein enthält SAM Domäne, welche notwendig für Repression durhc PcG ist
- ncPRC1 hat zinc-finger Domäne und RYBP oder YAF2, welche an PCGF1, 3, 5 oder 6 binden und somit ncPRC1.1, 1.3, 1.5 oder 1.6 bilden
- ncPRC1.1 ist einzige nicht-kanonischer Komplex, der auch in Drosophila gefunden werden konnte (dRAF)
- da RYBP/YAF2 schon in Choanoflagellata auftreten, CBX aber nicht, ist naheliegend, dass der nicht-kanonische Komplex in Evolution bereits früher auftrat


### CBX Proteine ###

- charakteristische Domäne ist die Chromodomäne
- Funktion (epigenetisch): H3K27me3 binding



- Bindungsaffinität zu H3K27me3 ist unterschiedlich, am höchsten bei CBX7
- CBX Proteine können je nach Zelltyp entweder onkogen oder tumorsuppressiv wirken


CBX2: 

- ist für sex determination wichtig, ist überexprimiert bei Brustkrebs

CBX4 (auch Pc2): 
- enthält zusätzlich zur Chromodomäne auch zwei SUMO-interactive motifs
- Sumoylierung ist ähnlich zu Ubiquitinierung, benötigt aktivierendes Enzym (E1), konjugierendes Enzym (E2) und Ligase (E3)
- CBX4 ist SUMO Ligase E3
- CBX4 sumoylates hnRNP K, einen p53 Kofaktor, der für transkriptionelle Regulation von p53 target genes benötigt wird


CBX7:

- ist in pluripotenten Zellen wichtigstes PRC1-assoziiertes CBX Protein, wichtig für die Aufrechterhaltung von Pluripotenz

CBX8: 

- fördert Proliferation, unterdrückt gleichzeitig Metastasierung bei Darmkrebs

### YAF2 und RYBP ###


- sind homologe Teile des PRC1 Komplex
- charakteristische Domäne ist die zinc finger Domäne
- Funktion (epigenetisch): DNA binding

RYBP: 
- ist zinc finger protein, spielt wesentliche Rolle bei Embryonalentwicklung, bindet Transkriptionsfaktoren und Polycom-Produkte sowie Ubiquitin und CBX Proteine
- liegt unstrukturiert vor, bis es an C-terminal Region von Ring1B (Polycomb Protein) oder an DNA bindet


YAF2 (YY1-assoziierter Faktor 2): 
- bindet an MYC und hemmt MYC-mediated transactivation und verstärkt MYCN-dependent transcriptional activation
- interagiert mit YY1, verstärkt dessen Abbau durch calpain
- Bestandteil von E2F5com-1 Komplex
- enthält einen RanBP2-type zinc finger

## Expression ##

nach Schuettengruber (2017) ist Expression der von uns zu bearbeitenden Proteine CBX Proteine ausschließlich in Metazoa zu erwarten, für RYBP/YAF2 zusätzlich auch bei Choanoflagellata

erwartet wird eine Expression von CBX Proteinen bei: Ctenophora, Porifera, Placozoa, Cnidaria und Bilateria

erwartet wird eine Expression von RYBP und YAF2 bei: Choanoflagellata, Ctenophora, Porifera, Placozoa, Cnidaria und Bilateria

| Taxa | Expression von |
| ------ | ------ |
| Choanoflagellata (unizellulär) | RYBP/ YAF2 |
| Ctenophora (Metazoa) | CBX2(4, 6-8); RYBP/ YAF2 |
| Porifera (Metazoa) | CBX2(4, 6-8); RYBP/ YAF2 |
| Placozoa (Metazoa) |CBX2(4, 6-8); RYBP/ YAF2 |
| Cnidaria (Metazoa) | CBX2(4, 6-8); RYBP/ YAF2 |
| Bilateria (Metazoa) | CBX2(4, 6-8); RYBP/ YAF2 |

