#!/bin/bash
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"
cd ..
awk -F"\t" 'NR>1{ print $8,"\t" $5 }' Proteome/proteomTable.tsv > Proteome/proteomTable.tmp.tsv
awk -F"\t" '!_[$2]++' Proteome/proteomTable.tmp.tsv > Proteome/proteomTable.dl.tsv
