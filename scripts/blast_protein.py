#!/bin/python3
import sys
import os
import glob
import subprocess
import tempfile
import pandas as pd
import ntpath
from filter_alignments import filter_alignments

proteom_cache_directory = '/home/dev/.cache/proteoms'
protein_cache_directory = '/home/dev/.cache/proteins'
out_dir = '/home/dev/.cache/'

# global variables like its 1999
df = pd.DataFrame(columns=["protein-key", "proteom-key", "e"])

def handle_alignment(term, filename, output):
    global out_dir
    output_dir = os.path.join(out_dir, "alignments")
    output_file = os.path.join(output_dir, "{term}-{target}.bin".format(term=term, target=os.path.splitext(os.path.basename(filename))[0]))
    with open(output_file, 'wb+') as file:
        file.write(output)
        file.close()

def handle_output(output, species_handle):
    for row in output.splitlines():
        tokens = row.split()
        if len(tokens) > 3:
            id_in = tokens[0]
            id_out = tokens[1]
            length = float(tokens[2])
            positive = float(tokens[3])
            bitscore = float(tokens[4])
            e_value = float(tokens[5])
            cov2 = float(tokens[6])
            cov = positive/length
            global df
            df = df.append({
                "protein-key": id_in,
                "proteom-key": id_out,
                "e":  float(e_value),
                "bit_score": float(bitscore),
                "percentage_of_alignment_without_gaps": float(cov),
                "query_coverage": float(cov2),
                "positive": float(positive),
                "length": float(length),
                "species_handle": species_handle
            }, ignore_index=True)

def get_proteom_paths():
    paths = []
    for root, dirs, files in os.walk(proteom_cache_directory):
        for file in files:
            if file.endswith(".faa"):
                 paths.append(os.path.join(root, file))
    return paths

def blast_term(term):
    term_file_path = '/home/dev/.cache/proteins/{term}.faa'.format(term=term)
    proteom_file_paths = get_proteom_paths()
    
    processes = []
    for file in proteom_file_paths:
        f = tempfile.TemporaryFile()
        g = tempfile.TemporaryFile()
        p = subprocess.Popen(['blastp', '-query', term_file_path, '-db',file, '-outfmt', '6 qseqid sseqid length positive bitscore evalue qcovs', '-evalue', '1e-5'],stdout=f)
        q = subprocess.Popen(['blastp', '-query', term_file_path, '-db',file, '-outfmt', '5','-evalue', '1e-5'],stdout=g)
        processes.append((p, q, f, g, file))

    for p, q, f, g, filename in processes:
        species_handle = os.path.splitext(os.path.basename(filename))[0]
        p.wait()
        q.wait()
        f.seek(0)
        g.seek(0)
        handle_output(f.read().decode("utf-8"), species_handle)
        handle_alignment(term, filename, g.read())

        f.close()
        g.close()
    global df
    global out_dir
    df = df.sort_values('e')
    #print(df)
    #df.to_csv(os.path.join(out_dir, "{term}.e.csv".format(term=term)))
    print(df)
    df = filter_alignments(df)
    print(df)
    df.to_csv(os.path.join(out_dir, "{term}.e.csv".format(term=term)))


def main():
    term = sys.argv[1]
    blast_term(term)

if __name__=='__main__':
    main()
