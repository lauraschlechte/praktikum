#!/bin/python3
import sys
import requests
import os
from lxml import html
from lxml import etree

term = sys.argv[1]
url = "https://www.ncbi.nlm.nih.gov/search/all/?term={term}"
query = url.format(term=term)
resp = requests.get(query)
if resp.ok:
    tree = html.fromstring(resp.text)
    print("Handling term: " + term)
    protein_link = tree.xpath('/html/body/main/div/section/div/div[1]/div/div/div/div/div/div[2]/div/div[2]/ul/li[2]/a')
    protein_link_address = list(protein_link[0].iterlinks())[0][0].get('href')
    r = requests.get(protein_link_address, allow_redirects=True)
    cont = r.content.decode("utf-8")
    with open('/home/dev/.cache/proteins/{term}.faa'.format(term=term), 'w') as term_file:
        term_file.write(cont)

else:
    print ("Boo! {}".format(resp.status_code))
    print (resp.text)
