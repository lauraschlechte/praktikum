#!/bin/bash
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"
mkdir -p ../output/.cache/proteins
mkdir -p ../output/.cache/proteoms
mkdir -p ../output/alignments
./create_download_proteome_tsv.sh
#./download_proteome.sh
#./extract_group_protein.sh Gruppe_2 | xargs -L 1 ./download_proteine_fasta.py
#./make_blast_dbs.sh
./extract_group_protein.sh Gruppe_2 | xargs -L 1 ./blast_protein.py 
