#!/bin/python3
import os
import subprocess
import tempfile
import json
from flask import Response
from flask import Flask
from flask_cors import CORS
import pandas as pd

base_path               = os.path.dirname(__file__)
output_base_path        = os.path.join(os.path.dirname(__file__),'../output/')
protein_cache_directory = os.path.join(output_base_path, '.cache/proteins')

app = Flask(__name__)
CORS(app)

@app.route("/proteins")
def index_proteins():
    script_path = os.path.join(base_path, 'extract_group_protein.sh')
    content = subprocess.check_output([script_path, "Gruppe_2"])
    tokens = content.decode("utf-8").split()
    return Response(json.dumps(tokens),  mimetype='application/json')

@app.route("/proteoms")
def index_proteoms():
    df = pd.read_csv(os.path.join(base_path, '../Proteome/proteomTable.tsv'), sep='\t', header=0)

    return Response(json.dumps(json.loads(df.reset_index().to_json(orient='records'))),  mimetype='application/json')

@app.route("/spiderchart/<proteom>")
def spiderchart(proteom):
    proteom_file = os.path.join(output_base_path, ".cache/proteoms/{proteom}.faa".format(proteom=proteom))
    proteom_ids = []
    with open(proteom_file, "r") as file:
        for line in file:
            if line.startswith(">"):
                tokens = line.split()
                proteom_ids.append(tokens[0][1:])
    
    script_path = os.path.join(base_path, 'extract_group_protein.sh')
    content = subprocess.check_output([script_path, "Gruppe_2"])
    proteins = content.decode("utf-8").split()
    protein_files = map(lambda x: os.path.join(output_base_path, "{protein}.e.csv".format(protein=x)), proteins)
    for protein_file in protein_files:
        df = pd.read_csv(protein_file)
        candidates = df.loc[df['proteom-key'].isin(proteom_ids)]
        best = candidates[candidates['e'] == candidates['e'].min()]
        print(protein_file)
        print(best)

    

    return Response(json.dumps([]), mimetype='application/json')

@app.route("/evalues/<protein>/<proteom>")
def evalue(protein=None, proteom=None):
    e_value_file_path = os.path.join(output_base_path, "{protein}.e.csv".format(protein=protein))
    df = pd.read_csv(e_value_file_path)
    

    return Response(json.dumps([]), mimetype='application/json')

app.run()
