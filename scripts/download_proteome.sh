#!/bin/bash
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"
cd /home/dev/.cache/proteoms/
awk -F"\t" '{print $1 $2}' /home/dev/bioinf_prak/praktikum/Proteome/proteomTable.dl.tsv | while read newfile location
do
	echo $newfile
	echo $location
	wget -O "${newfile}" $location
done

#cat ../Proteome/proteomTable.tsv  | tr "\t" "~" | cut -d"~" -f7,5 | egrep "[a-zA-Z0-9]*[.][a-zA-Z0-9]*" | uniq | xargs wget -P ../output/.cache/proteoms/ -O
#cd ../output/.cache/proteoms/
# Hacky shit bc some links don't comply to anything reasonable...
for f in *.gz ; do
  gunzip "$f" &> /dev/null || echo "Skipping file $f"
done
for f in *.zip ; do
  unzip "$f" &> /dev/null || echo "Skipping file $f"
done
#rm *.zip
#rm *.gz
