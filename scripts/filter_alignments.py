import pandas as pd

e_value_upper_bound     = 1e-6
coverage_lower_bound    = float(.9)

def filter_alignments(df):
    print(df['e'])
    filtered_e_values = df[df['e']<e_value_upper_bound]
    print(filtered_e_values)
    filtered_coverage = filtered_e_values[filtered_e_values['percentage_of_alignment_without_gaps']>coverage_lower_bound]
    print(filtered_coverage)
    return filtered_coverage

