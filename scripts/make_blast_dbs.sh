#!/bin/bash
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"
cd /home/dev/.cache/proteoms/
ls *.faa | xargs -L 1 makeblastdb -dbtype prot -in
