\documentclass{article}
\usepackage{fbox} 
\usepackage[a4paper, total={6.5in, 10in}]{geometry}
\usepackage{multicol}
\usepackage{lipsum}
\usepackage{tabularx}
\usepackage{xargs}                
\usepackage[table]{xcolor}% http://ctan.org/pkg/xcolor
\usepackage[colorinlistoftodos,prependcaption,textsize=tiny]{todonotes}
\newcommandx{\unsure}[2][1=]{\todo[linecolor=red,backgroundcolor=red!25,bordercolor=red,#1]{#2}}
\newcommandx{\change}[2][1=]{\todo[linecolor=blue,backgroundcolor=blue!25,bordercolor=blue,#1]{#2}}
\newcommandx{\info}[2][1=]{\todo[linecolor=OliveGreen,backgroundcolor=OliveGreen!25,bordercolor=OliveGreen,#1]{#2}}
\newcommandx{\improvement}[2][1=]{\todo[linecolor=Plum,backgroundcolor=Plum!25,bordercolor=Plum,#1]{#2}}
\newcommandx{\thiswillnotshow}[2][1=]{\todo[disable,#1]{#2}}
\setlength{\columnsep}{1cm}
\usepackage[utf8]{inputenc} %select encoding
\usepackage[T1]{fontenc} % T1 Schrift Encoding
\usepackage{lmodern} % Schriftfamilie lmodern
\usepackage[english]{babel}
\usepackage[babel]{csquotes}
\usepackage[backend=biber]{biblatex} %biblatex mit biber laden
\ExecuteBibliographyOptions{sorting=nyt, %Sortierung Autor, Titel, Jahr
bibwarn=true, %Probleme mit den Daten, die Backend betreffen anzeigen
isbn=false, %keine isbn anzeigen
url=false %keine url anzeigen
}
\addbibresource{main.bib} %Bibliographiedateien laden

\title{Exploring evolutionary history based on (some) PcG Proteins}

\author{\textbf{Group 2:} Elisa Israel, Dennis Kreußel, Laura Schlechte}
\date{\today}

\begin{document}

\maketitle
\section{Introduction}
	To get an insight into the evolution of different organisms this work investigates conversion and loss of epigenetic regulators on their stem lineage.
	To study this evolution we try to measure the phylogenetic distance between organisms. 
	In this context we chose organisms of vastly different complexity ranging from the \textit{Drosophila melanogaster} to \textit{Capsaspora owczarzaki}, which is the single celled ancestor of animals with the largest known protein synthesis expression.
	The phylogenetic distance is known to be correlated with epigenetic changes, but not genetic changes.\cite{skinner2014}
	Therefore, the proposed work focuses on protein sequence and domain analysis.

\section{Data}

Our given proteins of interest belong either to the family of CBX- or RYBP/YAF2-proteins. They are all part of the PRC1 complex (which is a repression complex) of the polycomb group. The polycomb group, as well as the trithorax group, is known to be conserved evolutionarily, which makes research regarding homology applicable. Both are chromatin-modifying and act antagonistically. PcG proteins form multi-protein complexes which are involved in the repression of gene expression by chromatin-based modification. In mammals the PRC1 complex consists of a canonic (cPRC1) and a noncanonic (ncPRC1) complex \cite{schuettengruber2017}. 

\subsection{CBX-Protein-Family}
The chromodomain is the characteristic feature for all proteins of the CBX-family, which belong to  cPRC1. Their epigenetic function lies in the binding of H3K27me. Furthermore all of the CBX proteins share the CBX7\_C domain. This group of proteins differ in their binding affinity, with CBX7 having  the highest one of all. Table 1 shows the main functions of the proteins assigned to us within the CBX-family \cite{schuettengruber2017}.

\begin{table}[h!]
  \bgroup
  \def\arraystretch{1.3}
  \begin{tabular}{|l|p{.89\textwidth}|} \hline
    protein & main functions/ characteristics \\ \hline
    CBX2    & - Important for sex determination \newline - Overexpressed in breast cancer\\ \hline
    CBX4    & 
      - Contains two SUMO interactive motifs in addition to the chromodomain \newline
      - Sumoylates hnRNP K, a p53 cofactor that is required for the transcriptional regulation of p53 target genes                              \\ \hline
    CBX6    &  - Function in gene regulation and in ESC differentiation still enigmatic                              \\ \hline
    CBX7    & - Most important PRC1-associated CBX protein in pluripotent cells \newline - Important for maintaining pluripotency \\ \hline
    CBX8    & - Promotes proliferation \newline - Suppresses metastasis in colon cancer\\  \hline                        
  \end{tabular}
  \caption{Main functions/ characteristics of selected proteins from the CBX-family (according to \cite{schuettengruber2017}).}
  \egroup
\end{table}  

\subsection{YY1-binding related proteins}
The remaining two proteins assigned to us are both YY1-binding related and belong to ncPRC1. A characteristic of this group is the zinc finger domain, with the epigenetic function lying in the binding of DNA. Table 2 describes the main functions of RYP and YAF2, both belonging to this protein family \cite{schuettengruber2017}.

\begin{table}[h!]
  \bgroup
  \def\arraystretch{1.3}
  \begin{tabular}{|l|p{.89\textwidth}|} \hline
    protein & main functions/ characteristics \\ \hline
    RYBP    & 
      - Zinc finger protein \newline 
      - Has essential role in embryonic development \newline 
      - Binds transcription factors and polycom products as well as ubiquitin and CBX proteins \newline 
      - Is unstructured until it binds to the C-terminal region of Ring1B (Polycomb Protein) or to DNA\\ \hline
    YAF2    & 
      - Binds to MYC and inhibits MYC-mediated transactivation \newline
      - Reinforces MYCN-dependent transcriptional activation \newline
      - Interacts with YY1 and increases its degradation by calpain \newline
      - Part of E2F5com-1 complex \newline
      - Contains a RanBP2-type zinc finger \\ \hline                      
  \end{tabular}
  \caption{Main functions/ characteristics of selected YY1-binding related proteins(according to \cite{schuettengruber2017}).}
  \egroup
\end{table}  

\section{Methods}
	\label{sec:methods}
\subsection{Preprocessing}
Today's centralized databases for real world protein data collected from experiments is susceptible to noisy, missing and inconsistent data.
Low-quality data can lead to low-quality results.
There is a number of data preprocessing techniques. 
Data Cleaning, Data Integration and Data Reduction and Data Transformation are the main concepts used widely in a vast majority of data centered research.
Data Integration and Data Transformations as concepts were not used in this work, since we neither used multiple datasources for unique datapoints nor did we require transformations during preprocessing.

\subsubsection{Data Cleaning}
Data Cleaning can be applied to remove noise and correct inconsistencies in data.
For the proteome data for every species of interest used in this project we removed proteins that where incompliant with the fasta format we interpreted them in. 
This meant that we removed proteins that used ids from sequence databases which occurred multiple times within the same proteome.
Furthermore we removed protein sequences from proteomes that contained characters in their sequence which are not valid codes for fasta protein sequences.

\subsubsection{Data Reduction}
Data Reduction can be applied to remove redundant features by aggregating or clustering. 
In this context we reduced the number of isoform protein sequences to consider to the first in the relevance hierarchy of NCBI.
This is mainly done to reduce complexity (and because we were advised to do so). 
This of course leads to our found homolog proteins being a lower bound of the full results since it would be possible to find more homologous when comparing to another isoform.

\subsection{Searching for homologous proteins}
Homology searches were performed with blastp, aligning the polycomb protein sequences against locally created databases from given proteome sequences.

\subsubsection{Filtering sequence alignments}
Unfortunately, even in the simplest scenarios, like the one described in this work, little information can be abstracted from alignment scores.
This is mainly due to the fact that the scoring metrics used by blast change based on the length of the sequences.

To filter the alignments found we used the e-value provided by blastp.
This e-value is metric for the expected number of alignments with a greater score that would be found by chance (evaluated against random sequences).
This of course implies that lower e-values are correlated with alignments of higher (probabilistic) significance.
As a threshold for the e-value we used $10^{-6}$.\cite{karlin1990}

We also considered additional constraints, e.g.one that forced the e-value to be below $10^{-6}$ while having a score larger than $150$. 
However, additional score or gap-count requirements did not increase the quality of the filtered alignments.

\subsubsection{Searching for homologs with HMMER}

Another tool for searching sequence databases for sequence homologs through protein domains is the software HMMER. On its website (\url{hmmer.org}) it is described as follows:

\begin{quote}
  "[HMMER] is often used together with a profile database, such as Pfam or many of the databases that participate in Interpro. HMMER is designed to detect remote homologs as sensitively as possible [...]. In the past, this strength came at significant computational expense, but as of the new HMMER3 project, HMMER is now essentially as fast as BLAST." \cite{hmmer2020} 
\end{quote}

HMMER most likely does not deliver the same results as BLAST. This is due to its design to detect remote homologs as sensitively as possible, relying on the strength of its underlying probability models. These models are so called profile hidden Markov models (profile HMMs), in contrast to BLAST using position-independent subsitution score matrices such as BLOSUM and PAM. The use of these profile HMMs allows it to set and interpret the large number of free parameters in a profile, including the position-specific gap and insertion parameters \cite{eddy1992}.\\
An exemplary search with HMMER (v3.3) could look as follows:

\begin{verbatim}
hmmsearch --tblout -E 1E-5 hmmer_CBX7C_AMIL CBX7C.hmm AMIL.faa
\end{verbatim}

In this example \texttt{hmmer\_CBX7C\_AMIL} is the generated output file, \texttt{CBX7C.hmm} is the downloaded hmm-file for our domain (from pfam) and \texttt{AMIL.faa} is our sequence file for the species with the ID "AMIL". Furthermore the threshold was adapted to \texttt{1E-5} because of statements made in the HMMER-manual:

\begin{quote}
  "If both E-values are significant (<<1), the sequence is likely to be homologous to your query. If the full sequence E-value is significant but the single best domain E-value is not, the target sequence is probably a multidomainremote homolog; but be wary, and watch out for the case where it’s just a repetitive sequence." \cite{eddy1992} 
\end{quote}

Regarding the output of \texttt{hmmsearch}, the result-files were merged (for each domain and each species) by domain into one output file for better overview. After that, the conversion of these four resulting files into csv-format was expedient in order to process the data more efficiently.

\subsection{Generating phylogenetic networks}
Given the alignments we generated with the blastp-tool, we created a multiple sequence alignment based on alignments with single protein using ClustalW2.
Lastly, using this multiple sequence alignment we separated family members by generating a phylogenetic network (tree based visualization). This network was created by the software SplitsTree5, which uses the NeighborNet method based on Hamming distances \cite{bryant2002}.

\newpage
\section{Results}
	\label{sec:results}
\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{treesCBX2color.png}
  \caption{\textbf{Splitstree Result for CBX2}
 {\small  \newline The figure shows a phylogenetic network created with the software Splitstree based on multiple sequence alignments created with Blast and ClustalW. The ID of each species is coloured according to the taxa. A red dot marks those species in which we did not expect the protein according to Schuettengruber (2017) (see \cite{schuettengruber2017}).
  \newline Both Bilateria species are shown to be adjacent, however, a neighbourhood to Cnidaria cannot be deduced. The branch of Cnidaria corresponds to the expectation. The only outlier is HVUL, which is also the most distant from the other species of the phylum in the given phylogenetic tree. NVEC, EPAL and ATEN as well as AMIL, SPIS and OFAV show a narrow split, which corresponds exactly to the representation in the given phylogenetic tree. Two species are not included in the tree because their alignments have been filtered out. The placozoa share a narrow split, which is largely detached from the rest of the tree, but adjacent to the Cnidaria branch. This is also in line with our expectations. The Porifera do not form a common branch, only the close relatives EMUE and AQUE share a narrow split. A striking feature is the occurrence of the Choanoflagellata, which share a narrow split according to their relationship. This can also be seen in the trees of the other CBX proteins and thus contradicts the paper by Schuettengruber. Furthermore, the occurrence of COWC and ATHA is noticable, since no CBX protein was expected in these species due to the large evolutionary distance.
  Since the network is wide spread, it is possible that paralogs were compared. 
  Due to the filter criteria during blasting, probably only one domain was compared, not the entire proteome.}}
\end{figure}

\newpage
\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{treesCBX4color.png}
  \caption{\textbf{Splitstree Result for CBX4}
  {\small  \newline The figure shows a phylogenetic network created with the software Splitstree based on multiple sequence alignments created with Blast and ClustalW. The ID of each species is coloured according to the taxa. A red dot marks those species in which we did not expect the protein according to Schuettengruber (2017) (see \cite{schuettengruber2017}).
  \newline Corresponding to the given phylogenetic tree, the branches for Bilateria, Cnidaria and Placozoa lie together in a wide split. The Bilateria share a short, narrow split. The Cnidaria are divided into three branches, where NVEC, ATEN, EPAL, OFAV and AMIL together lie in a long, narrow split. Further away from the rest of the Cnidaria is HVUL as expected. The two placozoa share a long, narrow split and are adjacent to Porifera species. These results are consistent with the expectation based on the given phylogenetic tree. Only AQUE and EMUE are far away from the other Porifera species. 
  Unexpected was the occurrence of Choanoflagellata as well as individual representatives of the Filasterea, Fungi and Viridiplantae. While the Choanoflagellata are grouped together in a wide split, the other outlier species are not in an expected order. Choanoflagellata can also be seen in the trees of the other CBX proteins and thus our results contradict the paper by Schuettengruber. COWC, ATHA and SCER are, due to the evolutionary distance, unexpected in this network. 
  The main branch can be divided in the middle, thus almost separating the expected and unexpected species from each other. 
  Since the network is wide spread, it is possible that paralogs were compared. 
  Due to the filter criteria during blasting, probably only one domain was compared, not the entire proteome.}}
\end{figure}

\newpage
\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{treesCBX6color.png}
  \caption{\textbf{Splitstree Result for CBX6}
    {\small  \newline The figure shows a phylogenetic network created with the software Splitstree based on multiple sequence alignments created with Blast and ClustalW. The ID of each species is coloured according to the taxa. A red dot marks those species in which we did not expect the protein according to Schuettengruber (2017) (see \cite{schuettengruber2017}).
    \newline Corresponding to the given phylogenetic tree, the branches for Bilateria, Cnidaria and Placozoa lie together in a wide split. The Bilateria share a short, narrow split. The Cnidaria share a long wide split, which separates into two wide branches, which, with the exception of OFAV, reflect expectation. HVUL shows a large distance to the other cnidaria, which is also reflected in the other CBX proteins. The two placozoa share a long split, which is very narrow and adjacent to the Cnidaria. These results are consistent with the expectation based on the given phylogenetic tree. The porifera are scattered over the network and therefore do not reflect the given phylogenetic tree, especially the outlier AQUE is striking. 
    Similarly to the other CBX networks, Choanoflagellata occur together in a split, contrary to the findings of Schuettengruber. Further outliers are COWC and ATHA, for which no hit was expected due to the evolutionary distance.
    Like in the CBX4 network, the main branch can be cut so that the expected and the unexpected species are separated from each other. However, this also separates the Porifera and HVUL outliers from the other expected species. 
    Since the network is wide spread, it is possible that paralogs were compared. 
    Due to the filter criteria during blasting, probably only one domain was compared, not the entire proteome.}}
\end{figure}

\newpage
\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{treesCBX7color.png}
    \caption{\textbf{Splitstree Result for CBX7}
    {\small  \newline The figure shows a phylogenetic network created with the software Splits
      on multiple sequence alignments created with Blast and ClustalW. The ID of each species is coloured according to the taxa. A red dot marks those species in which we did not expect the protein according to Schuettengruber (2017) (see \cite{schuettengruber2017}).
      \newline 
      The Bilateria share a short split in the neighbourhood of the Cnidaria. DMEL and the Porifera species OCAR are found in the same split, which should not occur due to their evolutionary distance. The Cnidaria and Placozoa span a wide branch and then split according to expectation. Only HVUL is, as with other CBX networks, an outlier far away from the other Cnidaria. The Placozoa are separated by a long narrow split, exactly as expected. The Porifera are scattered over the network and therefore do not reflect the given phylogenetic tree, but EMUE and AQUE share a short narrow split. 
      Choanoflagellata and representatives of Filasterea, Fungi and Viridiplantae are scattered throughout the network. The distribution does not reflect the given phylogenetic tree. Nevertheless, in contrast to the paper by Schuettengruber, the occurrence of chanoflagellata should be considered. The other marked species are evolutionary too far away to expect a hit here.
      Again, the main branch can be cut so that the expected and the unexpected species are separated from each other. However, this also separates part of the Porifera and HVUL from the other expected species. 
      Since the network is wide spread, it is possible that paralogs were compared. 
      Due to the filter criteria during blasting, probably only one domain was compared, not the entire proteome.}}
\end{figure}

\newpage
\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{treesCBX8color.png}
  \caption{\textbf{Splitstree Result for CBX8}
    {\small  \newline The figure shows a phylogenetic network created with the software Splitstree based on multiple sequence alignments created with Blast and ClustalW. The ID of each species is coloured according to the taxa. A red dot marks those species in which we did not expect the protein according to Schuettengruber (2017) (see \cite{schuettengruber2017}).
      \newline 
      The Bilateria share a short split in the neighbourhood of the Cnidaria and Placozoa. In the same split is also the species OCAR to be found, which should not occur due to its evolutionary distance. The Cnidaria span a wide branch and then split into long narrow splits, with HVUL a little further away from the rest. The placozoa separate by a long narrow split, matching the expectation. The porifera are scattered over the network and therefore do not reflect the given phylogenetic tree, but EMUE and AQUE share a short narrow split. 
      Choanoflagellata as well as COWC and ATHA are also found in the network. Similarly to the other CBX networks, Choanoflagellata occur together in a short wide split, contrary to the findings of Schuettengruber. With COWC and ATHA no hit was to be expected according to their distance.
      Also here the main branch can be cut, seperating the expected from the unexpected species. However, this also separates a part of the Porifera.
      Since the network is wide spread, it is possible that paralogs were compared. 
      Due to the filter criteria during blasting, probably only one domain was compared, not the entire proteome.}}
\end{figure}

\newpage
\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{treesRYBPcolor.png}
  \caption{\textbf{Splitstree Result for RYBP}
    {\small  \newline The figure shows a phylogenetic network created with the software Splitstree based on multiple sequence alignments created with Blast and ClustalW. The ID of each species is coloured according to the taxa. A red dot marks those species in which we did not expect the protein according to Schuettengruber (2017) (see \cite{schuettengruber2017}).
      \newline 
      Cnidaria are divided into two main branches, which correspond to the given phylogenetic tree. Outliers are DGIG and HVUL, which lie apart. In the immediate neighbourhood of the Cnidaria the placozoa are found in a long, very narrow split. The Porifera share a wide split as expected, OCAR was filtered out. In the split of the Porifera there is also HMIA, which should not be found in this split because of the large evolutionary distance. The network can be divided by a cut in Porifera, with the outlier HMIA, and Cnidaria or Placozoa.
      It is striking that no representative of the Chanoflagellata is found in the network, although this would be expected according to Schuettengruber. The same can also be observed in the network for the related YAF2. In contrast to the results of the CBX proteins, no unexpected species were found.
      Since the network is wide spread, it is possible that paralogs were compared. 
      Due to the filter criteria during blasting, probably only one domain was compared, not the entire proteome.}}
\end{figure}

\newpage
\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{treesYAF2color.png}
   \caption{\textbf{Splitstree Result for YAF2}
    {\small  \newline The figure shows a phylogenetic network created with the software Splitstree based on multiple sequence alignments created with Blast and ClustalW. The ID of each species is coloured according to the taxa. A red dot marks those species in which we did not expect the protein according to Schuettengruber (2017) (see \cite{schuettengruber2017}).
      \newline 
      Cnidaria are divided into two main branches and the single species DGIG, which corresponds to the given phylogenetic tree. As an outlier again HVUL can be found. In direct neighbourhood to the Cnidaria the placozoa are found in a long, narrow split. The Porifera share a wide split as expected, OCAR was filtered out. In the split of the Porifera there is also HMIA, which should not be found in this split because of the large evolutionary distance. The network can be divided by a cut in Porifera, with the outliers HMIA and HVUL, and Cnidaria or Placozoa.
      It is striking that no representative of the chanoflagellata is found in the network, although this would be expected according to Schuettengruber. The same can also be observed in the network for the related RYBP. In contrast to the results of the CBX proteins, no unexpected species were found.
      Since the network is wide spread, it is possible that paralogs were compared. 
      Due to the filter criteria during blasting, probably only one domain was compared, not the entire proteome.}}
\end{figure}

\newpage
\section{Discussion and Outlook}
	\label{sec:discussion}
A limitation in our workflow was that not all to be investigated species had a downloadable proteome. This is especially a problem with Ctenophora, because there was no proteome data available from any species for our analyses. As a result this strain was excluded from our investigations. For the Choanoflagellata the same proteome file was given for 19 species. In our further analyses these are all summarized under the abbreviation "HGRA". For future investigations, a differentiation into the 19 individual species could be included. 
Analysing the Splitstree results, problems arose if only one representative could be found for a group of several species (e.g. ATHA or COWC). These species then are often displayed distorted in the network due to a lack of close relatives. \\
Figure 8 shows our results in comparison to those of Schuettengruber. It is noticeable that no hits were to be expected at Choanoflagellata by Schuettengruber, but nevertheless we found some. In contrast, hits should have been expected for YAF2/RYBP by Choanoflagellata, which could not be confirmed in our analysis. As expected, we were able to find the proteins assigned to us mainly in the metazoa, even if there were outliers. This supports the hypothesis that the epigenetic regulatory functions arose in the development from unicellular to multicellular organisms.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.65\textwidth]{table.jpg}
  \caption{Comparison of our results to Schuettengruber}
\end{figure}


Further steps of our analysis would have been the deeper evaluation of the HMMER data, because different information than the findings by BLAST opens up here. This way we could verify our exisiting results. 
\\We decided that a dynamic display of our results would be a good choice to generate an easy and fast overview. Parts of this elaboration can be found in the appendix of this report. 
For further analyses, it should be examined how many paralogs per species are to be expected. In addition, the analyses can be carried out and compared with other isoforms of the proteins if possible.

\begin{figure}
  \begin{tabular}{|c|c|c|c|c|c|c|c|}
    \hline
    Species/Protein& CBX2 & CBX4 & CBX6 & CBX7 & CBX8 & YAF2 & RYBP \\
    \hline
    DMEL &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} & \cellcolor{blue!25}\\
    \hline
    HMIA &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} \\
    \hline
    NVEC &\cellcolor{blue!25} & \cellcolor{blue!25}&\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} \\
    \hline
    TKIT & & & & & & & \\
    \hline
    EPAL &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} \\
    \hline
    HVUL &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} \\
    \hline
    ATEN &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} & \cellcolor{blue!25}\\
    \hline
    AMIL &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} & \cellcolor{blue!25} \\
    \hline
    PDAM &\cellcolor{blue!25} & \cellcolor{blue!25}&\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} \\
    \hline
    SPIS &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} \\
    \hline
    OFAV &\cellcolor{blue!25} & \cellcolor{blue!25}&\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} \\
    \hline
    DGIG &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} & \cellcolor{blue!25}\\
    \hline
    MSQU && & & & & & \\
    \hline
    TADH &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} \\
    \hline
    TSPH &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} \\
    \hline
    AQUE &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} & \cellcolor{blue!25}\\
    \hline
    SCAR && & & & & & \\
    \hline
    XTES & & & & & & & \\
    \hline
    OCAR &\cellcolor{blue!25} & \cellcolor{blue!25}&\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} & & \\
    \hline
    EMUE &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} & \cellcolor{blue!25}\\
    \hline
    SCIL &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} \\
    \hline
    SROS &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} & & \\
    \hline
    HGRA &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} & & \\
    \hline
    MBRE &\cellcolor{blue!25} &\cellcolor{blue!25} & & &\cellcolor{blue!25} & & \\
    \hline
    COWC &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} & & \\
    \hline
    SCER &&\cellcolor{blue!25} & &\cellcolor{blue!25} & & & \\
    \hline
    ATHA &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} &\cellcolor{blue!25} & & \\
    \hline
    NCRA && & & & & & \\
    \hline
    SARC &&  & & & & & \\
    \hline
  \end{tabular}
  \caption{A heatmap of the protein distribution within the species of interest. A blue cell corresponds to a significant alignment being found for the given protein within the proteom of the species.}
\end{figure} 
\printbibliography

\newpage
\section{Workflow - Visualization}

\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{screenshots/tree.png}
  \caption{\textbf{Splitstree visualization on website}
    {\small  \newline The figure shows the starting state of a website we created to work with our results. At first the user selects a protein that the phylogenetic network shown is based upon. The user can zoom to have a better look at the structure at the root of the network.
	A legend of which species got introduced to the network because of a known or unknown aligned sequence (based on blastp output) is shown below.
	}}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{screenshots/alignment.png}
	\caption{\textbf{Alignment visualization on website}
    {\small  \newline The figure shows the state of the website where a protein is already selected to display a phylogenetic network. To understand why a species is shown in the network the user can select the species of interest.
	Afterwards all alignments found for this species proteom with regards to proteins in our database are shown below the network.
	This display can of course be further improved. E.g. the marking of known domains, and the information where the alignment is located on the sequences subject to the alignment may be interesting to the researcher.
	}}
\end{figure}


\begin{figure}[h!]
  \centering
  \includegraphics[width=.7\textwidth]{screenshots/full.png}
	\caption{\textbf{Full workflow visualization}
    {\small \newline The full website of which excerpts are shown.
	}}
\end{figure}

\end{document}
