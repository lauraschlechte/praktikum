import os, tempfile
import pandas as pd
from datetime import datetime
from flask import send_file
from flask_restful import Resource, abort
from Bio.Blast import NCBIXML
from Bio import SeqIO, AlignIO, Alphabet
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC
from blasting import filter
from Bio.Align.Applications import ClustalwCommandline


out_dir = '/home/dev/.cache/alignments/'

def phylogenetic_tree(name, alignments):
    timestamp = datetime.timestamp(datetime.now())
    filepath = "/tmp/{ts}.tmp".format(ts=name)
    out_filepath = "/tmp/{ts}.nexus".format(ts=name)
    aln_filepath = "/tmp/{ts}.aln".format(ts=name)
    with open(filepath, "w+") as file:
        count = SeqIO.write(alignments, file, "fasta")
    clustalw_cline = ClustalwCommandline("clustalw2", infile=filepath)
    clustalw_cline()
    AlignIO.convert(aln_filepath, "clustal", out_filepath, "nexus", alphabet=Alphabet.generic_protein)
    with open(aln_filepath, "r") as file:
        content = file.read()
        return content


def get_blast_result_paths(protein_key):
    paths = []
    for root, dirs, files in os.walk(out_dir):
        for file in files:
            if file.endswith(".bin") and file.startswith(protein_key):
                 paths.append(os.path.join(root, file))
    return paths


class TreeSVG(Resource):
    def get(self, protein_key):
        path = "trees/{key}.svg".format(key=protein_key)
        return send_file(path)


class Tree(Resource):
    def get(self, protein_key):
        blast_result_paths = get_blast_result_paths(protein_key)
        alignments = []
        names = []
        for file in blast_result_paths:
            try:
                with open(file) as blastfile:
                    records = NCBIXML.parse(blastfile)
                    df = pd.DataFrame()
                    for item in records:
                        for alignment in item.alignments:
                            for hsp in alignment.hsps:
                                if filter(hsp):
                                    df = df.append({
                                        "species": os.path.splitext(os.path.basename(file))[0].split("-")[1],
                                        "alignment": hsp.sbjct,
                                        "expect": hsp.expect,
                                        "length": hsp.gaps,
                                        "title": alignment.title,
                                    }, ignore_index=True)
                    df.sort_values(by=['expect'])
                    best_matching_row = df.iloc[0]
                    alignments.append(
                        SeqRecord(Seq(
                                best_matching_row['alignment'], 
                                IUPAC.protein), 
                                id=best_matching_row['species'],
                                name=best_matching_row['species'],
                                description=""
                        )
                    )
                    names.append({
                        "short": best_matching_row["species"],
                            "long": best_matching_row["title"]
                        })
            except Exception as err:
                pass

        #alignments.append(
        #    SeqRecord(Seq(hsp.sbjct, IUPAC.protein), id=os.path.splitext(os.path.basename(file))[0].split("-")[1],
        #              name=os.path.splitext(os.path.basename(file))[0].split("-")[1],
        #              description=" ".join(alignment.title.split(" ")[2:])))
        #print(df)
        #print(alignments)
        out = phylogenetic_tree(protein_key, alignments)
        return names
