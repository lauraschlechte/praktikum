import os
from tree import get_blast_result_paths
from Bio import SeqIO

proteom_dir = '/home/dev/.cache/proteoms/'

def lookup_proteom_key(proteom_key):
    proteom_filepath = os.path.join(proteom_dir, "{key}.faa".format(key=proteom_key))
    with open(file) as proteom_file:
        records = SeqIO.parse(proteom_file)
        print(records)
