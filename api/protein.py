import json
import os
from flask_restful import Resource
from Bio import SeqIO


def read_protein_fasta(name):
    records = []
    rec_dict = SeqIO.to_dict(SeqIO.parse("/home/dev/.cache/proteins/{key}.faa".format(key=name), "fasta"))
    for key in rec_dict.keys():
        record_dict = {
            "id": rec_dict[key].id,
            "name": rec_dict[key].name,
            "description": rec_dict[key].description,
            "features": rec_dict[key].features, 
            "seq": str(rec_dict[key].seq),
            "length": len(str(rec_dict[key].seq))
        }
        records.append(record_dict)
    return records

class Protein(Resource):
    def get(self, protein_key):
        expressions = read_protein_fasta(protein_key)
        return {
                "expressions": expressions
        }

class ProteinList(Resource):
    def get(self):
        proteins = []
        for root, dirs, files in os.walk("/home/dev/.cache/proteins/"):
            for file in files:
                if file.endswith('.faa'):
                    name = os.path.splitext(os.path.basename(file))[0]
                    expressions = read_protein_fasta(name)
                    proteins.append({
                        "name": name,
                        "expressions": expressions
                    })
        return proteins
