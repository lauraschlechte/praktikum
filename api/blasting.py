import json
import os
from flask_restful import Resource, abort
from Bio import SeqIO
from Bio.Blast import NCBIXML


def filter(hsp):
    is_filtered=True
    is_filtered &= (hsp.score > 150)
    is_filtered &= (hsp.gaps < 5)
    is_filtered &= (hsp.expect < 0.01)

    return True

class Blast(Resource):
    def get(self, species_key, protein_key):
        alignments=[]
        try:
            with open("/home/dev/.cache/alignments/{protein}-{proteom}.bin".format(protein=protein_key, proteom=species_key), "r") as file:
                records = NCBIXML.parse(file)
                for item in records:
                    for alignment in item.alignments:
                        for hsp in alignment.hsps:
                            if filter(hsp):
                            #if hsp.expect <0.01:
                                alignments.append({
                                    "title": alignment.title,
                                    "length": alignment.length,
                                    "score": hsp.score,
                                    "gaps": hsp.gaps,
                                    "expect": hsp.expect,
                                    "query": hsp.query,
                                    "match": hsp.match,
                                    "subject": hsp.sbjct
                                })
        except Exception as err:
            print(err)
            abort(404, description="No valid entry found for the queried blast result!")
        return {
            "subject": species_key,
            "query": protein_key,
            "alignment_count": len(alignments),
            "alignments": alignments
        }
