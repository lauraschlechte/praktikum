from flask import Flask, send_from_directory
from flask_restful import reqparse, abort, Api, Resource
from flask_cors import CORS
from protein import Protein, ProteinList
from species import Species, SpeciesList
from blasting import Blast
from tree import Tree, TreeSVG

app = Flask(__name__)
api = Api(app)
CORS(app)

class HelloWorld(Resource):
    def get(self):
        return {'hello': 'world'}

api.add_resource(HelloWorld, '/')
api.add_resource(ProteinList, '/proteins')
api.add_resource(Protein, '/proteins/<protein_key>')
api.add_resource(SpeciesList, '/species')
api.add_resource(Species, '/species/<species_key>')
api.add_resource(Blast, '/blasts/<species_key>/<protein_key>')
api.add_resource(Tree, '/tree/<protein_key>')
api.add_resource(TreeSVG, '/svg/<protein_key>')


if __name__ == '__main__':
    app.run(debug=True)
