import json
import os
from flask_restful import Resource
from Bio import SeqIO


def read_species_fasta(name):
    records = []
    try:
        rec_dict = SeqIO.to_dict(SeqIO.parse("/home/dev/.cache/proteoms/{key}.faa".format(key=name), "fasta"))
        for key in rec_dict.keys():
            record_dict = {
                "id": rec_dict[key].id,
                "name": rec_dict[key].name,
                "description": rec_dict[key].description,
                "features": rec_dict[key].features, 
                "seq": str(rec_dict[key].seq),
                "length": len(str(rec_dict[key].seq))
            }
            records.append(record_dict)
        return records
    except:
        return []

class Species(Resource):
    def get(self, species_key):
        expressions = read_species_fasta(species_key)
        return {
                "expressions": expressions
        }

class SpeciesList(Resource):
    def get(self):
        species = []
        for root, dirs, files in os.walk("/home/dev/.cache/proteoms/"):
            for file in files:
                if file.endswith('.faa'):
                    name = os.path.splitext(os.path.basename(file))[0]
                    #expressions = read_species_fasta(name)
                    species.append({
                        "name": name,
                    #   "expressions": expressions
                    })
        return species
