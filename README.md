# Praktikum

[Infos](./INFOS.md)

## Daily Reports

[Day1](./daily_reports/220620.md)

[Day2](./daily_reports/230620.md)

[Day3](./daily_reports/240620.md)

[Day4](./daily_reports/250620.md)

[Day5](./daily_reports/260620.md)

[Day6](./daily_reports/290620.md)

[Day7](./daily_reports/300620.md)
