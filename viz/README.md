# Viz

This module supplies our project with a website with all visualizations.

## Requirements

* display a philogenetic tree
* display a alignment length distribution for every species
* display statistics for the best alignments for every species
	* mark alignments in sequence
* mark domains in sequence
