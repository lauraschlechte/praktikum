import React from 'react';
import SpiderChart from "./SpiderChart";
import AlignmentChart from "./components/alignments/AlignmentChart";
import D3Wrapper from './components/D3Wrapper';
import './App.css'
import LengthDistribution from "./components/length_distribution/LengthDistribution";
import HeatMap from "./components/heatmap/HeatMap";
import PinchZoomPan from "react-responsive-pinch-zoom-pan";

class App extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state={
            "species": [],
            "proteins": [],
            "selected": null,
	    "selected_tree": null,
            "result": [],
	    "legend": [],
            "heatmap": {}
        }
        this.onChange = this.onChange.bind(this);
        this.onChangeTree = this.onChangeTree.bind(this);
    }

    componentDidMount() {
        fetch("http://localhost:5000/species").then(res => res.json()).then(data => {
            this.setState({
                "species": data
            })
        })
        fetch("http://localhost:5000/proteins").then(res => res.json()).then(data => {
            this.setState({
                "proteins": data
            })
		let iniobj = {}
		data.map(protein => {
			iniobj[protein.name] = []
		})
		this.setState({
			"heatmap": iniobj
		})
		data.map(protein => {
			fetch(`http://localhost:5000/tree/${protein.name}`).then(res => res.json()).then(data => {
				let shorts = data.map(d => d.short)
				let heatm = this.state.heatmap 
				heatm[protein.name] = shorts
				this.setState({
					"heatmap": heatm
				})
			})		
		})
        })
	
    }
    
    onChangeTree(e) {
        this.setState({
            "selected_tree": e.target.value,
        })
	fetch(`http://localhost:5000/tree/${e.target.value}`).then(res => res.json()).then(data => {
		this.setState({
			"legend": data
		})
	})
    }

    onChange(e) {
        this.setState({
            "selected": e.target.value,
            "result": []
        })
        this.state.proteins.map(protein => {
            fetch(`http://localhost:5000/blasts/${e.target.value}/${protein.name}`).then(res => res.json()).then(data => {
                this.setState({
                    "result": [...this.state.result, data]
                })
            })
        })
    }

    render() {
        return <div>
            <h1>
                Species Overview
            </h1>
 	    <h2>Phylogenetic Tree based on:</h2>
            <select onChange={this.onChangeTree} className={"select-wrap"}>{this.state.proteins.map((x) => <option key={x.name}>{x.name}</option>)}</select>
		<div className={"pan-zoom-container"}>
			<PinchZoomPan>
				<img src={this.state.selected_tree ? 
					`http://localhost:5000/svg/${this.state.selected_tree}`: 
					"http://localhost:5000/svg/CBX4"} 
		    		/>
			</PinchZoomPan>
		</div>
		<div className={"legend"}>
		    <ul>
		    {this.state.legend.map(obj => {
			return <li key={`${obj.short}`}>{obj.short}: {obj.long}</li>
		    })}
	    	    </ul>
		</div>
		    {/*<div className={"heatmap-wrapper"}>
                <HeatMap
                    proteins={this.state.proteins}
                    species={this.state.species}
	    	    data={this.state.heatmap}
                />
            </div>
	   */} 
	    <h2 className="select-head">Alignments for:</h2>
            <select onChange={this.onChange} className={"select-wrap"}>{this.state.species.map((x) => <option key={x.name}>{x.name}</option>)}</select>
            {/*<div className={"phylogenetic-tree-wrapper"}>
                <div className={"phylogenetic-tree-placeholder"}>
                    PLACEHOLDER - here will be a phylogenetic tree.
                </div>
            </div>
            */}
            <div>
                {this.state.result.map(res => (
                    <div key={`${res.subject}-${res.query}`}>
                    <h1>{res.subject} - {res.query}</h1>
                    {res.alignments.map((alignment,index) => (
                        <div key={`${res.subject}-${res.query}-${index}`}>
                            <h1>{alignment.title}</h1>
                            <h3>e: {alignment.expect}</h3>
                            <h3>length: {alignment.length}</h3>
                            <h3>score: {alignment.score}</h3>
                            <table className={"alignment-table"}>
                                <tbody>
                                    <tr>
                                        <td className={"heading-col"}>QUERY</td>
                                        {alignment.query.split("").map((char) => (
                                            <td>{char}</td>
                                        ))}
                                    </tr>
                                    <tr>
                                        <td className={"heading-col"}>MATCH</td>
                                        {alignment.match.split("").map((char) => (
                                            <td>{char}</td>
                                        ))}
                                    </tr>
                                    <tr>
                                        <td className={"heading-col"}>SUBJECT</td>
                                        {alignment.subject.split("").map((char) => (
                                            <td>{char}</td>
                                        ))}
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    ))}
                    </div>
                ))}
            </div>
            {/*<div className={"alignment"}>
            <D3Wrapper><AlignmentChart/></D3Wrapper>
            <D3Wrapper><LengthDistribution/></D3Wrapper>
 	    </div>*/}
        </div>;
    }
}

export default App;
