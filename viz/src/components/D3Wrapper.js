import React from 'react';
import AlignmentChart from './alignments/AlignmentChart';
import './D3Wrapper.css';

class D3Wrapper extends React.PureComponent {
	constructor(props) {
		super(props)
		this.state = {
			"dimensions": null
		}
		this.container = React.createRef();
		this.updateDimensions = this.updateDimensions.bind(this);
	}

	updateDimensions() {
		this.setState({
			"dimensions": {
				"width": this.container.current.clientWidth,
				"height": this.container.current.clientHeight
			}
		})
	}
	
	componentDidMount() {
		this.updateDimensions();
    		window.addEventListener('resize', this.updateDimensions);
  	}
	
  	componentWillUnmount() {
    		window.removeEventListener('resize', this.updateDimensions);
  	}

	renderContent() {
    		const { dimensions } = this.state;

    		return (
				React.cloneElement(this.props.children, {
						width:dimensions.width,
						height:dimensions.height })
		)
  	}

	render() {
		const { dimensions } = this.state;

		return (
      			<div className="d3-wrapper" ref={this.container}>
        			{dimensions && this.renderContent()}
      			</div>
		)
	}

}

export default D3Wrapper;

