import React from 'react';
import './HeatMap.css'

class HeatMap extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
	    this.props.proteins.map(protein => {
	   this.props.species.map(species => {
	   	console.log(this.props.data["CBX2"])
	   })
	    })
        return (
            <div className="zui-wrapper">
                <div className="zui-scroller">
                    <table className={"zui-table"}>
                        <thead>
                            <tr>
                                <td className={"zui-sticky-col"}></td>
                                {
                                    this.props.species.map(species => {
                                        return <th key={species.name} scope={"col"}>{species.name}</th>
                                    })
                                }
                            </tr>
                        </thead>
                        <tbody>
                        {
                                this.props.proteins.map(protein => {
                                    return <tr key={protein.name}>
                                        <th className={"zui-sticky-col"} scope={"row"}>{protein.name}</th>
                                        {this.props.species.map(species => {
                                            return <td key={`${protein.name}/${species.name}`} className={"center-text " + (this.props.data.hasOwnProperty(protein.name) ? ( this.props.data[protein.name].indexOf(species.name) > -1 ? "found":"not-found"): "not-found")}></td>
                                        })}
                                    </tr>
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default HeatMap;
