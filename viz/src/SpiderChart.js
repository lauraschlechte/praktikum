import React from 'react';
import Radar from 'react-d3-radar';
import './SpiderChart.css'

class SpiderChart extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        fetch("http://localhost:5000/spiderchart/"+this.props.proteom)
    }

    render() {
        return (
            <div className={"spiderchart-wrapper"}>
                <Radar
                    width={300}
                    height={300}
                    padding={50}
                    domainMax={1}
                    highlighted={null}
                    onHover={(point) => {
                        if (point) {
                            console.log('hovered over a data point');
                        } else {
                            console.log('not over anything');
                        }
                    }}
                    data={{
                        variables: this.props.proteins.map((protein) => (
                            {
                                key: protein,
                                label: protein
                            }
                        )),
                        sets: [
                            {
                                key: 'me',
                                label: 'Dog',
                                values: {
                                    CBX2: .3,
                                    CBX4: .2,
                                    CBX6: .1,
                                    CBX7: .7,
                                    CBX8: .6,
                                    RYBP: .8,
                                    YAF2: .2,
                                },
                            },
                        ],
                    }}
                />
            </div>
        );
    }
}

export default SpiderChart;
